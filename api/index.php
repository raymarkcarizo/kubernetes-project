<?php

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");

/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: GET");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");


// Function to sanitize input data
function sanitizeInput($data)
{
    return htmlspecialchars(strip_tags(trim($data)));
}


// Create connection
$conn = new mysqli('practical-exercise-db-service', 'root', 'root', 'todo');

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Handle GET request
if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // SELECT query for a "tasks" table
    $sql = "SELECT * FROM tasks";

    $result = mysqli_query($conn, $sql);

    // Check if the query was successful
    if ($result) {
        // Fetch all rows into an associative array
        $todos = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // response
        echo json_encode(['method' => 'GET', 'data' => $todos]);
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

# Use a PHP base image with Apache
FROM php:7.4-apache

# Install the mysqli extension
RUN docker-php-ext-install mysqli

# Set the working directory in the container
WORKDIR /var/www/html

# Copy the PHP application files to the container
COPY . /var/www/html/

# Add write permissions to the www-data user
RUN chown -R www-data:www-data /var/www/html
RUN chmod -R 755 /var/www/html

# Enabled mod_rewrite for RewriteEngine
RUN a2enmod rewrite

# Install any necessary dependencies (if required)
# For example, if you have a composer.json file, you can run:
# RUN composer install

# Expose the container port (default is 80 for Apache)
EXPOSE 80

# Start the Apache web server
CMD ["apache2-foreground"]
